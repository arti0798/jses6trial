//function with its lexical scope is known as closure

//1
// function x() {
//     var a = 7;

//     function y() {
//         console.log(a)
//     }

//     y()
// }

// x()
//--------------------------------------------------------------------------------
//2
// function x() {
//     var a = 7;

//     function y() {
//         console.log(a)
//     }

//     return y;
// }

// var z = x()

// console.log('z : ',z)

// z()

//ans is 7 because y always remember its lexical parent.
//---------------------------------------------------------------------------

// function x() {

//     var a = 10;

//     return function y() {
//         console.log(a)
//     }

// }

// var z = x()
// z()
// above same return the same result


function x() {
    var b = 90
   
    function y() {
        console.log(b)
    }
    b= 8080
    return y
}

var z = x()

z()
console.log(z)