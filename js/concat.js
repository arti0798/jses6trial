const arr1 = [1,2,3,4]
const arr3 = [11,22,33,44]
const arr = arr1.concat(arr3)

console.log(arr)

const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);

console.log(array3);

const num1 = [1,2,1]
const num2 = [11,22,3,3]
const num3 = [77,66,55,44]

const ar = num1.concat(num2,num3)

console.log(ar)