const { rejects } = require("assert");

function add(num1, num2) {
    let result = num1+num2
    console.log(result)
}

let num1=3;
let num2=2;
// add(num1, num2)



function addFun(num1, num2) {
    return new Promise((resolve, reject) => {
        console.log('NUM2 : ',num2)
        if(num1 & num2)
            return resolve(num1+num2)
        return reject("REJECT")    
    })
}

addFun(num1, num2).then(res => {
    console.log(res)
}).catch(err => {
    console.error(err)
})