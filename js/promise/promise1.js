const fs = require("fs")
function sendMessage(fileName) {

    return new Promise((resolve, rejects) => {
        fs.readFile(fileName, (error1, data1)=>{
            console.log("FILENAME : : ", fileName)
            if(error1) {
                return rejects('PROMISE REJECT............')
            }
            return resolve('PROMISE RESOLVE..............')
        })
    })
    
}


// function add(data, error) {
//     console.log(error, data);
// }

// const error = "error";
// const data = "data";

// add(error, data);

var fileName = 'a1.txt'
sendMessage(fileName).then(res => {
    console.log(res)
    sendMessage('b1.txt').then(res => {
        console.log(res)
    }).catch((err) => {
        console.error(err)
    })
}).catch((err) => {
    console.error("ERROR >>>> ", err)
})