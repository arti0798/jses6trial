const obj1 = {
    id: 1,
    name: 'Arti'
};

const obj2 = {
    address: 'Pune'
};


/**
 * OUTPUT:
 * {
 *  id: 1,
 *  name: 'Arti',
 *  address: 'Pune
 * }
 */

function updateObj(obj1, obj2) {
    const temp = {...obj1, ...obj2}
    console.log("RESULT : ",temp)
}
updateObj(obj1,obj2)