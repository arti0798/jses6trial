// ES6 in object (key: value)

let firstName = 'Arti';

const student = {
    firstName
};

console.log(student);

let key = 'fullName';
let name = 'Arti Singh';

// To use key from dynamic value use [key]
const student1 = {
    [key]: name
}

console.log(student1)