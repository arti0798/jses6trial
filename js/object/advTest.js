function updateObj(obj) {
    // const temp = obj
    // temp.name = "POOJA SINGH"
    // console.log('TEMP : ',temp.name)

    // const temp2 = {...obj}
    // temp2.name = "ANANAD SINGH"
    // console.log('TEMP2 : ',temp2.name)

    // const temp3 = {...obj.subject}
    // const temp3dup = {...temp3}
    // temp3dup.sub = "ANGULAR"
    // console.log("REMP3 : ",temp3dup);

    const temp4 = {
        ...obj, 
        subject: {
            ...obj.subject,
            teacher: {
                ...obj.subject.teacher,
                sirname: {
                    ...obj.subject.teacher.sirname
                }
            },
            mobile: [...obj.subject.mobile]
        }
    }
    temp4.subject.sub = 'Math';
    temp4.subject.teacher.name = "AZAD"
    temp4.subject.teacher.sirname.sr = "Singh"
    temp4.subject.mobile[0] = 19

    console.log('Temp4::', temp4);
    console.log("temp4.subject.teacher.sirname.sr : ",temp4.subject.teacher.sirname.sr)
}

const obj = {
    name : "ARTI SINGH",
    id: 2313,
    subject : {
        sub : "REACT JS",
        teacher: {
            name: "Anand",
            sirname: {
                sr: "SINGH"
            }
        },
        mobile: [123, 345345]
    }
}
// console.log(obj.name)
obj['name'] = "KHUSHI SINGH"
// console.log(obj.name)
updateObj(obj)
console.log('AFTER UPDATE : ',obj)
console.log("obj.subject.teacher.sirname.sr : ",obj.subject.teacher.sirname.sr)

