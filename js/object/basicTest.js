// const obj = {
//     company : 'SRV',
//     employee: ['Arti','Gauri','Manisha','Anand'],
//     obj2: {
//         project: "SIUX",
//         team: ['product','website']
//     },
//     getfun : function getManger() {
//         return "JAI"
//     }

// }

// console.log(obj.company)
// console.log(obj.employee[2])
// console.log(obj.obj2.team[1])
// console.log(obj.getfun())

const details = [
    {
        id:1,
        name:"Arti"
    },
    {
        id:2,
        name:"Azad"
    },
    {
        id:3,
        name:"Anand"
    }
]


// const temp = {...details, checked:true}

const temp = details.map((item)=> {
    return {...item, checked:true}
})

console.log(temp);