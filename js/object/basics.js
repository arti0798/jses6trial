const object = {}; // Object

/* 
Object has key value pair
key: rule _a-z0-9A-Z
value: function, number, string, array, object
*/

const student = {
    "firstName": 'Arti',
    "lastName": 'Singh',
    "age": 19,
    address: "Pune",
    getFullName: function getFullName() {
        return `${this.firstName} ${this.lastName}`
    },
    contact: {
        mob: '902123092',
        emergencyNum: '3454565646'
    },
    qualification: ['MCS', 'BCS', '12']
};

console.log(student.qualification[2])