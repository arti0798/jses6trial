
function updateValue(obj) {
    const temp1 = {...obj, 
        obj2 : {
            ...obj.obj2,
            obj3: {
                ...obj.obj2.obj3,
                arr : [...obj.obj2.obj3.arr]
            }
        }
    }
    temp1.lang = "NODE"
    temp1.obj2.lang2  = "ANGULAR"
    temp1.obj2.obj3.lang3 = "SPARK"
    temp1.obj2.obj3.arr[2] = 90
    console.log("TEMP! : ",temp1)
    console.log("TEMP!ARRAY  : ",temp1.obj2.obj3.arr)

}
const obj = {
    lang : "DJANGO",
    obj2 : {
        lang2 : "REACT",
        obj3 : {
            lang3 : "SCALA",
            arr : [1,11,111]
        }
    }
}

console.log("OBJ : ", obj)

updateValue(obj)
console.log("OBJ : ", obj)
console.log("OBJ : AARRAY: : ", obj.obj2.obj3.arr)

