// const str = "Anand";

// console.log(str);

// str = "AZad";
// console.log(str);

// const arr = [1,2, 3];
// console.log(arr);

// arr[0] = 100;
// console.log(arr);

// const obj = {
//     name: "Arti"
// };

// console.log(obj);
// obj.name = 'Anand';
// console.log(obj);


// Reference - [], {}
// Number, undefined, null, string


function updateObj(obj) {
    const tempObj = {...obj}; // Spread Operator
    tempObj.name = "Anand";

    tempObj.address.currAdd = "Banglore";
    console.log("Temp Obj", tempObj);

    const temp = {...obj['address']}
    tempObj.address.currAdd = "JIYANPUR";
    console.log("Temp Obj **", temp);


}

const user = {
    name: 'Arti',
    address: {
        perAdd: "UP",
        currAdd: "Pune"
    }
};
console.log(user);
updateObj(user);
console.log('ORIGINAL USER :: ',user);
