const arr = [21,43,54,24,99,10,34]

const maxValue = arr.reduce((prev, curr, index) => {
    if(prev > curr) {
        return prev;
    }
    return curr;
}, -9999);

console.log(maxValue)