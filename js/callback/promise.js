const fs = require('fs');

function readFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if(err) {
                console.error("Error::", err);
                return reject('File not found')
            }
            console.log('Reading file::', fileName);
            console.log(data.toString('utf-8'));
            resolve('File read successfully')
        })
    })
   
}

readFile('aqq.txt').then(res => {
    console.log('Result::', res);
}).catch(err => {
    console.log('Error::', err)
})

console.log('Last Line');

