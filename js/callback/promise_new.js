const fs = require('fs')
function read(fileName) {

    return new Promise((resolve, reject) => {
        fs.readFile(fileName,(err, data)=>{ 
            console.log("FILENAME : ", fileName);
            // console.log("DATA ", data.toString());
            if(err) {
                return reject('ERROR IN FILE')
            }
            if(data) {
            console.log("DATA ", data.toString());
            return resolve('SUCCESSFUL')

            }
        })
    })
    
}
let a = 'b.txt'
// console.log(fs.readFile(a,()=>{}));
read(a).then((res) => console.log(res)).catch(err => console.log(err));


