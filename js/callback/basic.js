function read(fileName, callback) {
    console.log('File Name::', fileName);
    for(let i = 0; i < 10; i++) {
        console.log(i);
    }
    callback('File read successfully');
}

function sendNotification(message) {
    console.log(message);
}

// function sendNotification1(message) {
//     console.log('Send Notification', message)
// }

const fileName = 'a.txt';

read(fileName, (message) => {
    console.log(message);    
});


read(fileName, sendNotification);