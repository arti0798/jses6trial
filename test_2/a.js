const arr = [1,1,2,1,1,2,2,2]
let count_1 = 0
let count_2 = 0

arr.map((item) => {
    if(item === 1) {
        count_1++
    }
    else if(item === 2) {
        count_2++
    }
})

if(count_1 > count_2) {
    console.log("1 wins");
}
else if(count_1 < count_2) {
    console.log("2 wins");
}
else if(count_1 === count_2) {
    console.log("tie");
}