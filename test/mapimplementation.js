const a = [1,2,33,44,55]


function map(a, mapFun){

    let arrMap = []

    for(let i =0; i < a.length;i++) {
        const result = mapFun(a[i],i)
        arrMap.push(result)
    }
    return arrMap
}


const res = map(a, function(item, index){
    return item*2
})

console.log(res);