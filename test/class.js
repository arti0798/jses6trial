class Student {
    constructor(name,rollno,grade,section){
        this.name = name
        this.rollno = rollno
        this.grade = grade
        this.section = section
    }

    getDetails() {
        return `name: ${this.name}, Roll No: ${this.rollno}, Grade: ${this.grade}, Section: ${this.section}`
    }
}

let stud = new Student("Arti",1,"A","B")
console.log(stud.getDetails());