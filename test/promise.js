function sumOfThreeNum(...elements) {
    console.log('>> ',elements)
    return new Promise((resolve, reject) => {
        if(elements.length > 3) 
            reject("ELEMENTS SHOLUD BE LESS THAN 3")
        else {
            let sum = 0;
            let i = 0;

            while(i<elements.length) {
                sum += elements[i]
                i++
            }
            resolve(sum)
        }    
    })
}

sumOfThreeNum(2,3,43).then((sum) => console.log("SUM ",sum)).catch(err => console.log("ERROR : ", err))